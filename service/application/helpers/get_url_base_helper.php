<?php

if ( ! function_exists('get_url_base'))
{
  function get_url_base()
  {

           $CI = & get_instance();
           $url      = base_url();
           
           $data['base_url']         = $url;
           $data['base_url_service'] = $url.'service/';           
           $data['base_url_static']  = $url.'static/';
           
          // $data['base_url_tod']  = $url .'tod/thumb/';
           return $data;
        
  }

}
?>