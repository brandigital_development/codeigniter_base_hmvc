<?php
class base_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


   function get($seed=1,$cant='0',$offset='0') {
           $this->db->select('*');
           $this->db->from('');
           $this->db->where('', '1');
           
           //$this->db->where('', $idtipo);
           $this->db->where_in('PRO_SRC_ID', $idtipo);
           $this->db->join('TIE_TIENDAS', 'TIE_TIENDAS.TIE_ID_USER = PRO_PROMOCIONES.PRO_USER_CREADOR');

           $this->db->order_by("PRO_SRC_ID DESC ,RAND(".$seed.")",'',FALSE);
           //$this->db->order_by("PRO_FECHA",'desc', FALSE);
           $this->db->limit($cant,$offset);
           
           $query = $this->db->get();
           //print_r($this->db->last_query());die();
           if ($query->num_rows() > 0)                
                return $query->result_array();

            return NULL;
        
    }

    function getSlug($slug) {
            $this->db->select('');
            $this->db->from('');
            $this->db->where('', $slug);            
            $this->db->limit(1);
           
            $query = $this->db->get();

            if ($query->num_rows() > 0)                
                return $query->row_array();

            return NULL;
        
    }

 


}
